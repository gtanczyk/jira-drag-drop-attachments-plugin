package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.jira.pageobjects.util.Tracer;

import javax.annotation.ParametersAreNonnullByDefault;

@ParametersAreNonnullByDefault
public abstract class FormDropZone extends DropZone
{
    @Override
    public void dropImage(final String fileName)
    {
        final Tracer tracer = traceContext.checkpoint();
        dropImage(fileName, tracer);
        traceContext.waitFor(tracer, "jira.issue.dnd.isclear");
        traceContext.waitFor(tracer, "jira.issue.dnd.uploadfinished");
    }
}
