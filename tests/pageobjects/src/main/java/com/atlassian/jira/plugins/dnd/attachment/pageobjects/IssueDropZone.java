package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

public class IssueDropZone extends DropZone {

    @ElementBy(cssSelector = ".issue-drop-zone[duiType='dndattachment/dropzones/IssueDropZone'].-dui-type-parsed")
    private PageElement dropZoneElement;

    @ElementBy(cssSelector = "#attachmentmodule")
    private PageElement dndPanel;

    public PageElement getDropZoneElement() {
        return dropZoneElement;
    }

    public By getProgressBarLocator() {
        return By.cssSelector(".issue-drop-zone[duiType='dndattachment/dropzones/IssueDropZone']~.upload-progress-bar");
    }

    public PageElement getContextElement() {
        return dndPanel;
    }
}