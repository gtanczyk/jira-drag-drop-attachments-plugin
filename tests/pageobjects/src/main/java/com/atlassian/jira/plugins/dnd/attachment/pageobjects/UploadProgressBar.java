package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;

public class UploadProgressBar
{

    private PageElement uploadProgressBar;

    public UploadProgressBar(final PageElement uploadProgressBar)
    {
        this.uploadProgressBar = uploadProgressBar;
    }

    public void cancel()
    {
        uploadProgressBar.find(By.ByCssSelector.cssSelector(".upload-progress-bar__control .aui-iconfont-close-dialog, .upload-progress-bar__upload-control span")).click();
        Poller.waitUntilFalse(uploadProgressBar.timed().isPresent());
    }

    public Status getStatus()
    {
        if (uploadProgressBar.hasClass("upload-progress-bar__upload-error"))
        {
            return Status.ERROR;
        }
        else if (uploadProgressBar.hasClass("upload-progress-bar__upload-finished"))
        {
            return Status.FINISHED;
        }
        else
        {
            return Status.UPLOADING;
        }
    }

    public boolean isPresent()
    {
        return uploadProgressBar.isPresent();
    }

    public static enum Status
    {
        UPLOADING,
        FINISHED,
        ERROR
    }
}
