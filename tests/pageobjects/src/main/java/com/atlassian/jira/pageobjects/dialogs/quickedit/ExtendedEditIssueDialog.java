package com.atlassian.jira.pageobjects.dialogs.quickedit;

public class ExtendedEditIssueDialog extends EditIssueDialog {
    public boolean isInFullMode() {
        return openFieldPicker().getMode() == FieldPicker.Mode.FULL;
    }
}
