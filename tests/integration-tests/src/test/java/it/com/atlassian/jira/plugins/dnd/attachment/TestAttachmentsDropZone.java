package it.com.atlassian.jira.plugins.dnd.attachment;

import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.config.DisableRTE;
import com.atlassian.jira.pageobjects.pages.viewissue.AddCommentSection;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachmentOptions;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachmentSection;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.AttachmentsDropZone;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.AttachmentsSection;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.test.categories.OnDemandAcceptanceTest;
import com.google.common.io.Files;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.functest.framework.FunctTestConstants.JIRA_USERS_ROLE_ID;
import static com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock.Sort.Direction.DESCENDING;
import static com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST;
import static com.atlassian.jira.permission.JiraPermissionHolderType.APPLICATION_ROLE;
import static com.atlassian.jira.permission.ProjectPermissions.CREATE_ATTACHMENTS;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest(WEBDRIVER_TEST)
@Category(OnDemandAcceptanceTest.class)
public class TestAttachmentsDropZone extends BaseWebdriverTest
{
    @Inject
    private TraceContext traceContext;

    @Override
    public void setUp()
    {
        super.setUp();
        backdoor.flags().disableFlags();
        issueKey = backdoor.issues().createIssue(PROJECT_KEY, "Empty attachments").key();
    }

    @After
    public void tearDown()
    {
        super.tearDown();
        backdoor.flags().enableFlags();
        backdoor.issues().deleteIssue(issueKey, true);
    }

    @Test
    public void testEmptyAttachments()
    {
        JIRA.goToViewIssue(issueKey);
        assertTrue("drop zone is visible", pageBinder.bind(AttachmentsDropZone.class).isVisible());

        attachTestImage();

        final List<String> fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();
        assertEquals("There is one attachment", 1, fileNames.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", fileNames.get(0).startsWith(Files.getNameWithoutExtension((TEST_FILE_NAME))));
        assertTrue("File extension is '" + Files.getFileExtension(TEST_FILE_NAME) + "'", fileNames.get(0).endsWith(Files.getFileExtension((TEST_FILE_NAME))));
    }

    @Test
    public void testViewModeToggle()
    {
        JIRA.goToViewIssue(issueKey);
        List<String> fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();

        assertTrue("drop zone is visible", pageBinder.bind(AttachmentsDropZone.class).isVisible());
        assertEquals("There are no attachments", 0, fileNames.size());

        attachTestImage();
        pageBinder.bind(AttachmentSection.class).openOptions().setViewMode(AttachmentsBlock.ViewMode.LIST);
        attachTestImage();
        getAttachmentOptions().setViewMode(AttachmentsBlock.ViewMode.GALLERY);
        attachTestImage();
        getAttachmentOptions().setViewMode(AttachmentsBlock.ViewMode.LIST);
        fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();
        assertEquals("There are 3 attachments", 3, fileNames.size());
    }

    private AttachmentOptions getAttachmentOptions() {
        return pageBinder.bind(AttachmentSection.class).openOptions();
    }

    private void attachTestImage() {
        pageBinder.bind(AttachmentsDropZone.class).attachImage(TEST_FILE_NAME);
    }

    @Test
    public void testSortByToggle()
    {
        final String firstFileName = "aaa_file.png";
        final String secondFileName = "bbb_file.png";

        JIRA.goToViewIssue(issueKey);
        List<String> fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();

        assertTrue("drop zone is visible", pageBinder.bind(AttachmentsDropZone.class).isVisible());
        assertEquals("There are no attachments", 0, fileNames.size());

        Tracer tracer = traceContext.checkpoint();
        final AttachmentsDropZone attachmentsDropZone1 = pageBinder.bind(AttachmentsDropZone.class);
        attachmentsDropZone1.dropImage(firstFileName, null);
        waitForAttachmentsDropZoneToNotBeVisible(attachmentsDropZone1);
        getAttachmentOptions().setSortBy(AttachmentsBlock.Sort.Key.DATE);
        traceContext.waitFor(tracer, "jira.issue.dnd.attached");

        tracer = traceContext.checkpoint();
        final AttachmentsDropZone attachmentsDropZone = pageBinder.bind(AttachmentsDropZone.class);
        attachmentsDropZone.dropImage(secondFileName, null);
        waitForAttachmentsDropZoneToNotBeVisible(attachmentsDropZone);
        getAttachmentOptions().setSortOrder(DESCENDING);
        traceContext.waitFor(tracer, "jira.issue.dnd.attached");

        fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();
        assertEquals("There are 2 files", 2, fileNames.size());

        assertEquals("Last uploaded file is first on the list", secondFileName, fileNames.get(0));
    }

    private void waitForAttachmentsDropZoneToNotBeVisible(AttachmentsDropZone attachmentsDropZone) {
        waitUntilFalse(attachmentsDropZone.getFullPageDropZone().timed().isVisible());
    }

    @Test
    public void shouldCancelAttachmentAfterFailedUpload()
    {
        JIRA.goToViewIssue(issueKey);
        assertTrue("drop zone is visible", pageBinder.bind(AttachmentsDropZone.class).isVisible());

        breakAttachmentStore();

        Tracer tracer = traceContext.checkpoint();
        AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME);
        traceContext.waitFor(tracer, "jira.issue.dnd.commit.fail");
        Poller.waitUntilFalse(dropZone.getFullPageDropZone().timed().isVisible());

        dropZone.getProgressBar(TEST_FILE_NAME).cancel();

        assertEquals("Drop zone is empty", 0, dropZone.getFileNames().size());

        tracer = traceContext.checkpoint();
        dropZone.dropImage(TEST_FILE_NAME);
        traceContext.waitFor(tracer, "jira.issue.dnd.commit.fail");

        // restore correct attachment settings, so it does not break attachment panel
        fixAttachmentStore();
        waitForAttachmentsDropZoneToNotBeVisible(dropZone);
        getAttachmentOptions().setSortOrder(DESCENDING);

        dropZone = pageBinder.bind(AttachmentsDropZone.class);
        dropZone.getProgressBar(TEST_FILE_NAME).cancel();

        assertEquals("Drop zone is empty", 0, dropZone.getFileNames().size());
    }

    @Test
    public void whenUnifiedAttachmentsEnabledFullDropzonePresent()
    {
        JIRA.goToViewIssue(issueKey);
        final AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        assertTrue(dropZone.getFullPageDropZone().isPresent());
    }

    @Test
    public void whenUnifiedAttachmentsEnabledDropZoneAppearsWhenDragoverIssue()
    {
        JIRA.goToViewIssue(issueKey);
        final AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        final Tracer tracer = traceContext.checkpoint();
        dropZone.dragoverIssue();
        traceContext.waitFor(tracer, "jira.issue.dnd.dragover");
    }

    @Test
    @DisableRTE
    public void whenUnifiedAttachmentsEnabledWhenEditingFieldDropAddsMarkup()
    {
        final ViewIssuePage issuePage = JIRA.goToViewIssue(issueKey);
        final AddCommentSection comment = issuePage.comment();
        final AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        dropZone.dropOntoFullDropzone(TEST_FILE_NAME);
        // The textfield isn't populated until it has focus, so manually give it focus
        comment.typeInput("");
        assertEquals("Comment should be filled with wiki markup for image",
                "!testfile.png|thumbnail!", comment.getInput().trim());
    }

    @Test
    public void whenUnifiedAttachmentsEnabledCannotDropWithoutPermission()
    {
        backdoor.permissionSchemes().removeProjectRolePermission(schemeId, CREATE_ATTACHMENTS, JIRA_USERS_ROLE_ID);
        backdoor.permissionSchemes().removePermission(schemeId, CREATE_ATTACHMENTS, APPLICATION_ROLE.getKey())
                .parameter("")
                .getRequest();
        JIRA.goToViewIssue(issueKey);
        final AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        assertFalse(dropZone.getFullPageDropZone().isPresent());
    }

    @Test
    public void whenUnifiedAttachmentsEnabledCannotFullPageDropWhenDialogOpen()
    {
        final ViewIssuePage issuePage = JIRA.goToViewIssue(issueKey);
        issuePage.editIssue();
        final AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        final Tracer tracer = traceContext.checkpoint();
        dropZone.dragoverIssue();
        traceContext.waitFor(tracer, "jira.issue.dnd.dropnotallowed");
    }
}
