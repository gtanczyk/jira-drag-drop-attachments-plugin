package it.com.atlassian.jira.plugins.dnd.attachment;

import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.dialogs.quickedit.CreateIssueDialog;
import com.atlassian.jira.pageobjects.dialogs.quickedit.EditIssueDialog;
import com.atlassian.jira.pageobjects.elements.GlobalMessage;
import com.atlassian.jira.pageobjects.framework.util.JiraLocators;
import com.atlassian.jira.pageobjects.model.DefaultIssueActions;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.*;
import com.atlassian.jira.testkit.client.restclient.Attachment;
import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.testkit.client.restclient.IssueLink;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.test.categories.OnDemandAcceptanceTest;
import com.google.common.io.Files;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

@WebTest ({ WEBDRIVER_TEST})
@Category ({ OnDemandAcceptanceTest.class })
public class TestDialogs extends BaseWebdriverTest
{
    @javax.inject.Inject
    protected TraceContext traceContext;

    @Inject
    PageElementFinder elementFinder;

    @Test
    public void testAttachFileLinkExists() throws Exception {
        final String issueKey = backdoor.issues().createIssue(projectId, ISSUE_SUMMARY).key();
        assertThat(JIRA.goToViewIssue(issueKey).getIssueMenu().isItemPresentInMoreActionsMenu("Attach files"), is(true));
    }

    @Test
    public void testCreateIssueDialog() throws Exception {
        Tracer tracer = traceContext.checkpoint();
        final CreateIssueDialog createIssueDialog = pageBinder.bind(JiraHeader.class).createIssue();
        Poller.waitUntilTrue("CreateIssueDialog was not opened.", createIssueDialog.isOpen());
        createIssueDialog.selectProject(PROJECT_NAME);

        traceContext.waitFor(tracer, "jira.issue.dnd.issuedropzone.render");

        DropZone dropZone = pageBinder.bind(CreateIssueDialogDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME);

        List<String> images = dropZone.getFileNames();
        assertEquals("One image on upload list", 1, images.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", images.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", images.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));

        final String issueSummary = "My Summary";
        createIssueDialog.fill("summary", issueSummary).submit(GlobalMessage.class);

        final String issueKey = JIRA.backdoor().search().getSearch(new SearchRequest().jql(String.format("text~\"%s\"", issueSummary))).issues.get(0).key;

        verifyOneAttachmentAddedToIssue(issueKey);
    }

    private void verifyOneAttachmentAddedToIssue(String issueKey)
    {
        JIRA.goToViewIssue(issueKey);

        List<String> fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();
        assertEquals("There is one attachment", 1, fileNames.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", fileNames.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", fileNames.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));
    }

    @Test
    public void testEditIssueDialog() throws Exception {
        JIRA.goToViewIssue(issueKey).getIssueMenu().invoke(DefaultIssueActions.EDIT);

        int attachmentCount = ((List<Attachment>) backdoor.issues().getIssue(issueKey).fields.get("attachment")).size();

        final EditIssueDialog editIssueDialog = pageBinder.bind(EditIssueDialog.class);

        DropZone dropZone = pageBinder.bind(EditIssueDialogDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME);

        List<String> images = dropZone.getFileNames();
        assertEquals("One image on upload list", 1, images.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", images.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", images.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));

        editIssueDialog.submit();
        assertTrue("Dialog was closed after submit", editIssueDialog.isClosed().byDefaultTimeout());

        JIRA.goToViewIssue(issueKey);

        List<String> fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();
        assertEquals("There is one attachment", attachmentCount + 1, fileNames.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", fileNames.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", fileNames.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));
    }

    @Test
    public void testEditIssuePage() throws Exception {
        final Issue issue = backdoor.issues().getIssue(issueKey);
        int attachmentCount = ((List<Attachment>) issue.fields.get("attachment")).size();

        final EditIssueDetailsPage editIssuePage = JIRA.goTo(EditIssueDetailsPage.class, issueKey, issue.id);

        DropZone dropZone = pageBinder.bind(EditIssuePageDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME);

        List<String> images = dropZone.getFileNames();
        assertEquals("One image on upload list", 1, images.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", images.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", images.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));

        editIssuePage.submit();

        List<String> fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();
        assertEquals("There is one attachment", attachmentCount + 1, fileNames.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", fileNames.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", fileNames.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));
    }


    @Test
    public void testCreateSubtaskDialog() throws Exception {
        JIRA.goToViewIssue(issueKey).getIssueMenu().invoke(DefaultIssueActions.CREATE_SUBTASK);

        final CreateIssueDialog createIssueDialog = pageBinder.bind(CreateIssueDialog.class, CreateIssueDialog.Type.SUBTASK);

        DropZone dropZone = pageBinder.bind(CreateSubtaskDialogDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME);

        List<String> images = dropZone.getFileNames();
        assertEquals("One image on upload list", 1, images.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", images.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", images.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));

        createIssueDialog.fill("summary", "My Summary").submit(IssueCreatedMessage.class);

        final List<IssueLink.IssueLinkRef> subtasks = backdoor.issues().getIssue(issueKey).fields.subtasks;
        JIRA.goToViewIssue(subtasks.get(subtasks.size() - 1).key());

        final AttachmentsSection attachmentsSection = pageBinder.bind(AttachmentsSection.class);

        List<String> fileNames = attachmentsSection.getFileNames();
        assertEquals("There is one attachment", 1, fileNames.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", fileNames.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", fileNames.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));
    }

    @Test
    public void testAttachmentFieldDescription()
    {
        final String TEST_DESCRIPTION = "test description";
        backdoor.getTestkit().fieldConfiguration().changeFieldDescription("Default Field Configuration", "attachment", TEST_DESCRIPTION);

        final CreateIssueDialog createIssueDialog = pageBinder.bind(JiraHeader.class).createIssue();

        final String divDescription = createIssueDialog.find(By.cssSelector("div.file-input-list div.description")).getText();

        assertThat("Field description contains test description", divDescription, Matchers.containsString(TEST_DESCRIPTION));

        backdoor.getTestkit().fieldConfiguration().changeFieldDescription("Default Field Configuration", "attachment", "");
    }
}
