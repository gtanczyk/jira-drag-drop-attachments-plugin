define('dndattachment/progressbars/AttachmentsUploadProgressBar', ['require'], function(require) {
    var UploadProgressBar = require('dndattachment/progressbars/UploadProgressBar');
    var analytics = require('jira/analytics');
    var flag = require('jira/flag');
    var $ = require('jquery');
    var escapeHtml = require('dndattachment/aui/escape-html');
    var DnDTemplates = require('dndattachment/templates');
    var I18n = require('dndattachment/i18n');
    function queueEvent(name, props) {
        analytics.send({ name: 'issue.dnd.attachment.attachmentsuploadprogress.'+name, data: props || {} });
    }

    return UploadProgressBar.extend({
        render: function() {
            this.$node.html(DnDTemplates.AttachmentsUploadProgressBar({ isImageType: this.isImageType() }))
                    .removeClass('upload-progress-bar')
                    .addClass('attachments-upload-progress-bar');

            this.$node.toggleClass('attachment-content', !this.isImageType());

            this.afterRender();
        },

        loadThumbnail: function(file) {
            if(this.$node.data("viewMode") == "gallery") {
                if(UploadProgressBar.prototype.isImageType.call(this)) {
                    return this._super.apply(this, arguments);
                } else {
                    return $(JIRA.Templates.ViewIssue.renderThumbnailIcon({ mimetype: file.type })).appendTo(this.getThumbnailNode());
                }
            } else {
                return $(JIRA.Templates.ViewIssue.renderAttachmentIcon({ mimetype: file.type })).appendTo(this.getThumbnailNode());
            }
        },

        showErrorMessage: function(message, status) {
            if(status == "abort") {
                // upload was aborted by hand, so we don't show flag
                return;
            }

            var errorFlag = flag.showErrorMsg(I18n("dnd.attachment.not.uploaded")(this.getFileName()), escapeHtml(message));
            $(errorFlag).on('aui-flag-close', function() {
                this.destroy();
            }.bind(this));

            this.bind('onDestroy', function() {
                errorFlag.close();
            });
        },

        isImageType: function() {
            return this.$node.data("viewMode") == "gallery";
        },

        getControlNode: function () {
            return this.$node.find('.upload-progress-bar__upload-control span');
        },

        getAutoDestroy: function() {
            return false;
        }
    });
});
