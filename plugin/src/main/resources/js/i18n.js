// because almond.js breaks jsI18n transformers

define('dndattachment/i18n', ['require'], function(require) {
    var formatter = require('jira/util/formatter');
    var dict = {
        "dnd.attachment.unsupported.browser": formatter.I18n.getText("dnd.attachment.unsupported.browser"),
        "dnd.attachment.file.is.too.large": formatter.I18n.getText("dnd.attachment.file.is.too.large"),
        "dnd.attachment.upload.in.progress": formatter.I18n.getText("dnd.attachment.upload.in.progress"),
        "dnd.attachment.not.uploaded": function(param) {
            return formatter.I18n.getText("dnd.attachment.not.uploaded", param)
        },
        "dnd.attachment.unauthorized": formatter.I18n.getText("dnd.attachment.unauthorized"),
        "dnd.attachment.internal.server.error": formatter.I18n.getText("dnd.attachment.internal.server.error"),
        "dnd.attachment.upload.aborted": formatter.I18n.getText("dnd.attachment.upload.aborted"),
        "dnd.attachment.upload.remove": formatter.I18n.getText("dnd.attachment.upload.remove")

    };
    return function(key) {
        if(dict[key])
            return dict[key];
        else
            throw "Unknown key: "+key;
    };
});