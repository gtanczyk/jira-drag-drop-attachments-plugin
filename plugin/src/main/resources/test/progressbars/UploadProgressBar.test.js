AJS.test.require([
    "jira.webresources:require-shim",
    "jira.webresources:jira-global",
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:drag-and-drop-attachment-javascript"
], function(){
    "use strict";

    var $ = require('jquery');
    var DnDTemplates = require('dndattachment/templates');
    var InlineAttach = require('jira/attachment/inline-attach');
    var UploadProgressBar = require('dndattachment/progressbars/UploadProgressBar');

    module("TestIssueDropZone", {
        setup: function() {
            this.sandbox = sinon.sandbox.create();
        },

        teardown: function() {
            this.sandbox.restore();
        }
    });

    test("UploadProgressBar test", function() {
        var $fixture = $('#qunit-fixture');
        var node$ = $('<div></div>').appendTo($fixture);

        var renderSpy = this.sandbox.spy(UploadProgressBar.prototype, "render");

        var instance = new UploadProgressBar(node$);

        ok(renderSpy.calledOnce, "render called once");
    });

    test("UploadProgressBar uploadFile test", function(assert) {
        var done = assert.async();
        var uploadStub = this.sandbox.stub(InlineAttach.AjaxUpload.prototype, "init", function(options) {
            this.options = options;
        });
        var requestStub = this.sandbox.stub(InlineAttach.AjaxUpload.prototype, "upload", function() {
            this.options.success({ id: 1, name: 'newfilename.zip' }, 200);
        });
        var monitorStub = this.sandbox.stub(UploadProgressBar.prototype, "monitorUpload");

        var file = { name: 'testfilename.zip' };

        var $fixture = $('#qunit-fixture');
        var $node = $fixture.html(DnDTemplates.UploadProgressBar({}));
        $node.data("file", file);

        var instance = new (UploadProgressBar.extend({
            $node: $node,
            monitorUpload: monitorStub,
            loadThumbnail: this.sandbox.stub(),
            reportError: this.sandbox.stub(),
            getUploadParams: this.sandbox.stub(),
            checkSession: function() {
                var result = new $.Deferred();
                result.resolve();
                return result;
            }
        }))($node);

        UploadProgressBar.prototype.uploadFile.apply(instance, [file]).then(function() {
            ok($node.find('.upload-progress-bar__file-name').html() == 'newfilename.zip', 'file-name filled');
            ok(monitorStub.calledOnce, 'monitorUpload called once');
            done();
        });
    });

    test("UploadProgressBar monitorUpload test", function() {
        var $fixture = $('#qunit-fixture');
        var $node = $fixture.html(DnDTemplates.UploadProgressBar({}));
        var progressBar = $node.find('.upload-progress-bar__progress-bar');

        var instance = new (UploadProgressBar.extend({ init: function() { }, $node: $node }))($node);

        var deferred = new $.Deferred();
        instance.monitorUpload(deferred);

        deferred.notify(false);
        ok(progressBar.hasClass('UploadProgressBar_progressUnknown'), 'UploadProgressBar_progressUnknown class exists');

        deferred.notify(0.5);
        ok(!progressBar.hasClass('UploadProgressBar_progressUnknown'), 'UploadProgressBar_progressUnknown class does not exist');
        var $bar = progressBar.find('.upload-progress-bar__bar');

        // second condition just for jQuery compatibility
        ok($bar.css('width') === '50%' || ($bar.width() / $bar.parent().width() === 0.5), 'bar width is 50%');

        deferred.resolve();
        ok($node.hasClass('upload-progress-bar__upload-finished'), 'upload-progress-bar__upload-finished class exists');
    });

    test("UploadProgressBar uploadLimit test", function(assert) {
        var done = assert.async();
        var $fixture = $('#qunit-fixture');
        var $node = $fixture.html(DnDTemplates.UploadProgressBar({}));

        var instance = new (UploadProgressBar.extend({
            $node: $node,
            loadThumbnail: this.sandbox.stub(),
            reportError: this.sandbox.stub(),
            getUploadParams: this.sandbox.stub()
        }))($node);

        UploadProgressBar.prototype.uploadFile.call(instance, { size: 1001 }, 1000).then(function() {
            ok(false);
            done();
        }, function() {
            ok(true);
            done();
        })
    });

    test("UploadProgressBar uploadFilenameXSS test", function() {
        var $fixture = $('#qunit-fixture');
        var $node = $fixture.html(DnDTemplates.UploadProgressBar({}));
        var instance = new (UploadProgressBar.extend({
            $node: $node,
            loadThumbnail: this.sandbox.stub(),
            reportError: this.sandbox.stub(),
            getUploadParams: this.sandbox.stub()
        }))($node);

        var spy = sinon.spy($.prototype, "text");
        var messageXss = 'this is message <script>alert("xss")</script>';
        UploadProgressBar.prototype.showErrorMessage.call(instance, messageXss, '');

        ok(spy.calledOnce);
        ok(spy.calledWith(messageXss), 'Message should be html escaped using $.text()');
        $.prototype.text.restore();
    });
});

