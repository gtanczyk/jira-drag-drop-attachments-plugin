package com.atlassian.jira.plugins.dnd.attachment.providers;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import com.google.common.collect.ImmutableMap;
import webwork.config.Configuration;

import java.util.Map;
import java.util.Optional;

import static com.atlassian.jira.config.properties.APKeys.JIRA_ATTACHMENT_SIZE;
import static com.atlassian.jira.config.properties.APKeys.JIRA_OPTION_ALLOWTHUMBNAILS;
import static java.util.Optional.ofNullable;

/*
 * This class provides the dnd-metadata webpanel with some metadata that
 * is consumed by this plugin and by other plugins.
 */
public class MetadataContextProvider extends AbstractJiraContextProvider
{

    private final AttachmentService attachmentService;

    public MetadataContextProvider(AttachmentService attachmentService)
    {
        this.attachmentService = attachmentService;
    }

    @Override
    public Map getContextMap(ApplicationUser applicationUser, JiraHelper jiraHelper)
    {
        ImmutableMap.Builder<String, Object> builder = ImmutableMap.builder();

        Project project = jiraHelper.getProject();

        // if project is null, canAttach returns false
        builder.put("canAttach", ofNullable(canAttach(applicationUser, project)).orElse(false).toString());

        if (project != null && project.getProjectTypeKey() != null)
        {
            builder.put("projectType", project.getProjectTypeKey().getKey());
        }
        else
        {
            builder.put("projectType", "");
        }

        builder.put("uploadLimit", Configuration.getString(JIRA_ATTACHMENT_SIZE));
        builder.put("thumbnailsAllowed", ComponentAccessor.getApplicationProperties().getOption(JIRA_OPTION_ALLOWTHUMBNAILS));

        return builder.build();
    }

    private Boolean canAttach(ApplicationUser applicationUser, Project project)
    {
        JiraServiceContext context = new JiraServiceContextImpl(applicationUser, new SimpleErrorCollection());
        return attachmentService.canCreateAttachments(context, project);
    }
}
