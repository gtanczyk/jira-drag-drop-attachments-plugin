package com.atlassian.jira.plugins.dnd.attachment;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.plugin.viewissue.AttachmentBlockContextHelper;
import com.atlassian.jira.plugin.viewissue.AttachmentViewModeOptionsFactory;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.preferences.UserPreferencesManager;
import com.atlassian.jira.util.velocity.VelocityRequestContextFactory;

public class AttachmentViewModeOptionsFactoryProxy extends AttachmentViewModeOptionsFactory
{
    public AttachmentViewModeOptionsFactoryProxy(
            final VelocityRequestContextFactory requestContextFactory,
            final JiraAuthenticationContext authenticationContext,
            final ApplicationProperties applicationProperties,
            final VelocityRequestContextFactory velocityRequestContextFactory,
            final UserPreferencesManager userPreferencesManager,
            final PermissionManager permissionManager,
            final IssueManager issueManager)
    {
        super(requestContextFactory,
                authenticationContext,
                applicationProperties,
                new AttachmentBlockContextHelper(
                        velocityRequestContextFactory,
                        applicationProperties,
                        issueManager,
                        permissionManager,
                        userPreferencesManager,
                        authenticationContext));
    }
}
